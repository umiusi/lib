(function(){
    /*
     * ドラッグ＆ドロップ機能追加
     *   使用方法：ドラッグ＆ドロップをしたい要素に「class='drag-and-drop'」を付加する。
     *   https://q-az.net/elements-drag-and-drop/
	 *
	 *   使用例
	 *   http://lite.runstant.com/?v=0.0.3#lVRfb9xEEP8qW7dRk/b8J8mdm/uXB1DfEA+8cjys7fXdlj3b8q4vOU4n1XdQSMNDi1SqCFVBoipECFRBn4CED2MuSZ/4Csyu7cZHoNAXe2dm5zczO7+ZiTYiMadhoLU0y7CMTa2mcSIEDfpaa6IJKhgBUzbfy+ZH2XyezZ7/+ds9Jf4gxfnj06PD8/nx+e/Hi/tfI0YdpKP3koALHAjA8gh3YxqJPMLp3SdZ+nGWHmbpN/I728vSgyx98PLL/bODXwwEcBJ39lwFO8zmP8tz+v3LJ0eLh8fZ7AvDMADUTxgDWEIA08eMk5rmJnFMIGJLG4gh06agCT0iS1CyLGUckdJc00aYJVLsXPFCV5qQNGz3AtQLOuURdQYEe+oE5yERGLkDHMP7dHtaInx9q6chc8ke4CEB44iSnSiMBdjdMBCQGSh3qCcGXY+MqEt0JdRQwkmscxcz7DDSDcJ/A8RRxIg+DB0Kvx3i6KDQXRxJt6UgY8IrGAWSauP2tYn6TztmLv9DmEq7llCvTSqW6eUAXIxVAPWHALlcGpUj4rELSAMhIt4yTcdxvcDoUzHA7oeGGw7NZEgTTk2gkBnjHbOBvaaLG1YDb25uNm3Hdm2X3MJ1e927Zdn2lt2wrKZrW6YX476OA0/34jAy7kD525CAipk30XzVxY4TemM4FYl5dIRchjmXlVdRoEDqgTImnu6Eu4AozR0THLb/r68DBCucwfRmvmPCWLjzKnQYle5LL1rAmWaW3s/Skyx9mCuU6r8GFoYpn9ksfQyjuPjku/Nn6dkLEH88+/bXxf6jxcnn2expNj+QUzj/DKZw8emzswf3Ko4H2Wz/j5P9MiojAslyUBfBUCVDoI7RJ+I2I/LI3xq/LWt+F5i2eqnutXaJ4ofxqkSiXavGuvKawUjQF4M2Ytu0jejNm2gNTcrrPWEqBrxPPzAU64BLLIzz5vW0dv4Yj7LZUV7H+d5Pi6d7p1+9KAEQKt2x590eQabvUA60JzFkOQxhQBPIr4b8JHAl+1fXJjAXPIRILOyvXi+uICJdr1xfa0/hrlxKFyVNi1Tz5l1wM2dnwUmgab525PJSlVS2lst5ZWndqKEbLYfASxF5wr4gcf4iwBid049ge7fgHHuwXEDVhtDTXiDx82s+DLa8R1rI3jAaK21ll4nkdvWELXS1Xq+rIhyY0X4cJtCuwjTgbNWqIWulhpr2yloO0AsksxWCWm+ohdYtK9pVGANC+wOxpKqksVHqBNkVOma0H7SQCy9KYqVmNCB6DlFFKBP1fT9PAaGrxdCWFPl77kher2/5znq7aJAII5VWgSmJ7IuqJsctB/p1wHbdafj2Em7jEm5jGfdi2F+bsk+srY0l5OYl5GYFWdFIMa3Cozt4hAvlBZ206XT6Fw==
     */
    document.addEventListener("DOMContentLoaded", function(event) {

      {
        /*
         * 必要なCSSルールを追加
         *   (lib化のため、JSによるCSSスタイル生成)
         *   http://takuya-1st.hatenablog.jp/entry/20120914/1347582729
         */
        //CSSノード追加
        var newStyle = document.createElement('style');newStyle.type = "text/css";
        document.getElementsByTagName('head').item(0).appendChild(newStyle);
        css = document.styleSheets.item(0)
        //追加
        var idx = document.styleSheets[1].cssRules.length;
        css.insertRule(".drag-and-drop { cursor: move; position: absolute; z-index: 1000; }", idx );
        css.insertRule(".drag { z-index: 1001; }", idx + 1 );
      }

      //対象要素の取得
      var elements = document.getElementsByClassName("drag-and-drop");
      //要素内のクリックされた位置を取得するグローバル（のような）変数
      var x;
      var y;
  
      //マウスが要素内で押されたとき、又はタッチされたとき発火
      for(let i = 0; i < elements.length; i++) {
          elements[i].addEventListener("mousedown", mdown, false);
          elements[i].addEventListener("touchstart", mdown, false);
      }
    });
  
    //マウスが押された際の関数
    function mdown(e) {

        //クラス名に .drag を追加
        this.classList.add("drag");

        //タッチデイベントとマウスのイベントの差異を吸収
        var event;
        if(e.type === "mousedown") {
            event = e;
        } else {
            event = e.changedTouches[0];
        }

        //要素内の相対座標を取得
        x = event.pageX - this.offsetLeft;
        y = event.pageY - this.offsetTop;

        //ムーブイベントにコールバック
        document.body.addEventListener("mousemove", mmove, false);
        document.body.addEventListener("touchmove", mmove, false);
    }

    //マウスカーソルが動いたときに発火
    function mmove(e) {

        //ドラッグしている要素を取得
        var drag = document.getElementsByClassName("drag")[0];

        //同様にマウスとタッチの差異を吸収
        var event;
        if(e.type === "mousemove") {
            event = e;
        } else {
            event = e.changedTouches[0];
        }

        //フリックしたときに画面を動かさないようにデフォルト動作を抑制
        e.preventDefault();

        //マウスが動いた場所に要素を動かす
        drag.style.top = event.pageY - y + "px";
        drag.style.left = event.pageX - x + "px";

        //マウスボタンが離されたとき、またはカーソルが外れたとき発火
        drag.addEventListener("mouseup", mup, false);
        document.body.addEventListener("mouseleave", mup, false);
        drag.addEventListener("touchend", mup, false);
        document.body.addEventListener("touchleave", mup, false);

    }

    //マウスボタンが上がったら発火
    function mup(e) {
        var drag = document.getElementsByClassName("drag")[0];

        //ムーブベントハンドラの消去
        document.body.removeEventListener("mousemove", mmove, false);
        drag.removeEventListener("mouseup", mup, false);
        document.body.removeEventListener("touchmove", mmove, false);
        drag.removeEventListener("touchend", mup, false);

        //クラス名 .drag も消す
        drag.classList.remove("drag");
    }

})()