/*
 * メッセージファイル出力スクリプト
 * メッセージ一覧xlsx2file.js
 *
 *   このスクリプトは、設計書「メッセージ一覧.xlsx」からメッセージファイルを生成するスクリプトです。
 *
 *   使い方
 *     このスクリプトをダブルクリック等で実行すると、
 *     メッセージプロパティファイル仕様書.xlsx
 *     を元に
 *     メッセージファイルを生成します。
 *
 */
(function() {
	/*
	 * 定数
	 */
	// 入力ファイル関連
	var IN_FILENAME = '../メッセージ一覧.xlsx';			// 入力ファイル名
	var IN_SHEET_NAME = 'メッセージ一覧';	//入力シート名
	var IN_START_ROW = 10;	//入力開始行

	// 出力ファイル関連
	var OUT_FILENAME = './application_ja.properties';	// メッセージファイル名
	var BR = '\n\r';	//改行コード
	
	
	
	/*
	 * 入力ファイル存在チェック
	 */
	var objFso = new ActiveXObject('Scripting.FileSystemObject');
	if ( !objFso.FileExists( IN_FILENAME ) ){
		//入力ファイルが存在しない場合、処理中断
		WScript.Echo('異常終了:\n'
					+ IN_FILENAME + 'ファイルが存在しません。\n'
					+ 'このスクリプトファイル内の定数の値を変更して下さい。');
		objFso = null;
		WScript.Quit();
	}
	
	

	/*
	 * メイン処理
	 *   設計書「メッセージ一覧.xlsx」からメッセージファイルを生成
	 */
	
	var objExcel = new ActiveXObject('Excel.Application');
	var inExcel = objExcel.Workbooks.Open(objFso.GetAbsolutePathName(IN_FILENAME));
	inExcel.CheckCompatibility = false;
	objExcel.EnableEvents = false;
	objExcel.ScreenUpdating = false;
	
	
	// 設計書「メッセージ一覧.xlsx」からデータ抽出
	var bunrui = '';
	
	var out_data = '';
	var i = IN_START_ROW;
	var inSheet = inExcel.Sheets(IN_SHEET_NAME);

	while ( inSheet.Cells(i, 'K').Value ) {
	// メッセージ文言に記載がなくなるまで下記処理を行う。
		
		// 大項目コメント(分類)
		if ( bunrui !== inSheet.Cells(i, 'B').Value ) {
			
			if ( i !== IN_START_ROW ) out_data += BR;
			
			out_data += '################################################################################' + BR;
			out_data += '## ' + inSheet.Cells(i, 'B').Value + BR;
			out_data += '################################################################################' + BR;
			
			bunrui = inSheet.Cells(i, 'B').Value;
		}
		
		// コメント
		if ( inSheet.Cells(i, 'AD').Value ) {
			// 説明に記載あればコメントとして出力
			out_data += '#' + inSheet.Cells(i, 'AD').Value;
			
			// 引数説明に記載あればコメントとして出力
			if ( inSheet.Cells(i, 'X').Value　&& inSheet.Cells(i, 'X').Value !== '－' ) {
				out_data += ' ' + inSheet.Cells(i, 'X').Value;
			}
			out_data += BR;
		}
		
		// メッセージデータ
		out_data += inSheet.Cells(i, 'B').Value + '.'
					+ inSheet.Cells(i, 'D').Value + '.'
					+ inSheet.Cells(i, 'I').Value + '='
					+ inSheet.Cells(i, 'K').Value + BR;
		
		i++;
	}
	
	inSheet = null;
	inExcel.Close();
	inExcel = null;
	
	// ファイルへ出力
	writeFile(OUT_FILENAME, out_data, 'utf-8');

	/*
	 * 後処理
	 */
	objExcel.EnableEvents = true;
	objExcel.ScreenUpdating = true;
	objExcel.Quit();
	objExcel = null;
	
	objFso = null;
	
	WScript.Echo('正常終了');



	/*
	 * ファイルの保存
	 */
	function writeFile(fname, text, charset) {
	  if (charset == undefined) {
		charset = '_autodetect_all';
	  }
	  var adTypeBinary = 1, adTypeText = 2;
	  var adSaveCreateNotExist = 1, adSaveCreateOverWrite = 2;
	  var adWriteLine = 1;
	  var s = new ActiveXObject('ADODB.Stream');
	  s.Type = adTypeText;
	  var utf8nobomFlg = false;
	  if(charset.indexOf('utf-8') > -1){
		if(charset.length > 5){
		  //使用例:writeFile(OUT_FILENAME_DOC1, out_data_doc1, 'utf-8nobom');
		  s.Charset = 'utf-8';
		  utf8nobomFlg = true;
		}
	  }else{
	    s.Charset = charset;
	  }
	  s.Open();
	  s.WriteText(text, adWriteLine);
	  
	  if(utf8nobomFlg){
	  //UTF-8(BOMなし)用
		// バイナリモードにするためにPositionを一度0に戻す
		// Readするためにはバイナリタイプでないといけない
		s.Position = 0;
		s.Type = adTypeBinary;
		// Positionを3にしてから読み込むことで最初の3バイトをスキップする
		// つまりBOMをスキップします
		s.Position = 3;
		var bin = s.Read();
		s.Close();

		// 読み込んだバイナリデータをバイナリデータとしてファイルに出力する
		var stm = new ActiveXObject("ADODB.Stream");
		stm.Type = adTypeBinary;
		stm.Open();
		stm.Write(bin);		  
		
		stm.SaveToFile(fname, adSaveCreateOverWrite);
		stm.Close();
	  }else{
	  //UTF-8(BOMなし)以外用
	    s.SaveToFile(fname, adSaveCreateOverWrite);
	    s.Close();
	  }
	}

})();
