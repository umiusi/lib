(function(global){
  'use strict';

  //山札
  class Talon {
  	//スート(suit)
    get SPADE(){return '1';}
  	get HEART(){return '2';}
  	get DIA(){return '3';}
  	get CLUB(){return '4';}
  	get JOKER(){return '0';}

    constructor(){
      this.cards=[
    			this.SPADE + '01',
    			this.SPADE + '02',
    			this.SPADE + '03',
    			this.SPADE + '04',
    			this.SPADE + '05',
    			this.SPADE + '06',
    			this.SPADE + '07',
    			this.SPADE + '08',
    			this.SPADE + '09',
    			this.SPADE + '10',
    			this.SPADE + '11',
    			this.SPADE + '12',
    			this.SPADE + '13',

    			this.HEART + '01',
    			this.HEART + '02',
    			this.HEART + '03',
    			this.HEART + '04',
    			this.HEART + '05',
    			this.HEART + '06',
    			this.HEART + '07',
    			this.HEART + '08',
    			this.HEART + '09',
    			this.HEART + '10',
    			this.HEART + '11',
    			this.HEART + '12',
    			this.HEART + '13',

    			this.DIA + '01',
    			this.DIA + '02',
    			this.DIA + '03',
    			this.DIA + '04',
    			this.DIA + '05',
    			this.DIA + '06',
    			this.DIA + '07',
    			this.DIA + '08',
    			this.DIA + '09',
    			this.DIA + '10',
    			this.DIA + '11',
    			this.DIA + '12',
    			this.DIA + '13',

    			this.CLUB + '01',
    			this.CLUB + '02',
    			this.CLUB + '03',
    			this.CLUB + '04',
    			this.CLUB + '05',
    			this.CLUB + '06',
    			this.CLUB + '07',
    			this.CLUB + '08',
    			this.CLUB + '09',
    			this.CLUB + '10',
    			this.CLUB + '11',
    			this.CLUB + '12',
    			this.CLUB + '13'
    		];
    }

    shuffle(){
      var i, j, tmp, length;
   
      for (length = this.cards.length, i = length - 1; i > 0; i--) {
          j = Math.floor(Math.random() * (i + 1));
          tmp = this.cards[i];
          this.cards[i] = this.cards[j];
          this.cards[j] = tmp;
      }
    }

		pull(){
		  var card=this.cards[0];
		  this.cards.shift();
		  return card;
		}

    //残数
		getZun(){
		  return this.cards.length;
		}

    //1 or 2
		setJoker(num){
		  if(num===1 || num===2) this.cards.push(this.JOKER);
		  if(num===2) this.cards.push(this.JOKER);
	  }

		reset(){
		  this.constructor();
		}
  }

  //手札
	class UserCards {
	  //手札
    constructor(){
  	  this.cards=[];
    }

    inCard(card){
      this.cards.push(card);
    }

    outCard(param){
      var card = 'not found outCard param = ' + param;
      if(typeof(param)==='number'){
      //何番目
        if(0 < param && param <= this.cards.length){
    		  card=this.cards[param - 1];
    		  this.cards.splice(param - 1, 1);
        }
      }else if(typeof(param)==='string'){
      //このカード
        var pos = this.cards.indexOf(param);
  		  card=this.cards[pos];
  		  this.cards.splice(pos, 1);
      }

      return card;
    }

    //残数
		getZun(){
		  return this.cards.length;
		}

    exist(card){
      return (this.cards.indexOf(card) > -1);
    }
	}


  if(typeof global.Cards === 'undefined'){
    global.Cards={};
  }
  global.Cards.Talon=Talon;
  global.Cards.UserCards=UserCards;
})(window);

// window.onload = function() {

//   //山札テスト
//   {
//     var talon = new window.Cards.Talon();
//   console.log(talon.cards);
//   console.log(talon.getZun());
//     //console.log(talon.pull());
//     // talon.setJoker(1);
//     // talon.shuffle();
//     // talon.setJoker(2);
//     // talon.shuffle();
//   // console.log(talon.cards);
//   // console.log(talon.getZun());
//     //talon.reset();
//   // console.log(talon.cards);
//   // console.log(talon.getZun());
//   }

//   //手札テスト
//   {
//     //配る
//     var user1 = new window.Cards.UserCards();
//     var user2 = new window.Cards.UserCards();
  
//     user1.inCard(talon.pull());
//   console.dir(user1.cards);
//   console.log(user1.getZun());
  
//     user1.inCard(talon.pull());
//   console.dir(user1.cards);
//   console.log(user1.getZun());

//   // console.log(user1.outCard(2));
//   // console.dir(user1.cards);
//   // console.log(user1.getZun());
  
//   console.log(user1.outCard('102'));
//   console.dir(user1.cards);
//   console.log(user1.getZun());
  
//   console.log(talon.cards);
//   console.log(talon.getZun());


  
//     user2.inCard(talon.pull());
//   console.dir(user2.cards);
//   console.log(user2.getZun());
  
//     user2.inCard(talon.pull());
//   console.dir(user2.cards);
//   console.log(user2.getZun());

//   console.log(user2.outCard(2));
//   console.dir(user2.cards);
//   console.log(user2.getZun());
  
//   // console.log(user2.outCard('104'));
//   // console.dir(user2.cards);
//   // console.log(user2.getZun());
  
//   console.log(talon.cards);
//   console.log(talon.getZun());
  
//   console.dir(user1.cards);
//   console.log(user1.getZun());

//   }


//   return;
// }
