/*
 * メッセージ一覧出力スクリプト
 * メッセージ一覧_file2xlsx.js
 *
 *   このスクリプトは、メッセージプロパティファイルから設計書を生成するスクリプトです。
 *   詳細は、「メッセージプロパティファイル仕様書.xlsx」の「メッセージプロパティファイル共通仕様シート」を参照下さい。
 *
 *   使い方
 *     初回は、自分の環境に合わせて環境変数を設定し直してください。
 *
 *     このスクリプトをダブルクリック等で実行すると、
 *     環境変数で設定されているソースルートパスにある
 *     アプリケーション・メッセージプロパティファイル と ラベル・メッセージプロパティファイル
 *     を元に
 *     メッセージプロパティファイル仕様書.xlsx
 *     を生成します。
 *
 */
(function() {
	/*
	 * 環境変数 ※自分の環境に合わせて設定し直してください。
	 */
	var src_rootFullPath = "C:/JavaProg/pleiades/workspace/FSP/src";	// ソースルートパス(＝メッセージプロパティファイルのパス)
	
	
	
	/*
	 * 定数
	 */
	// 入力ファイル関連
	var APPLICATION_JA_PROPERTIES_FILENAME = "application_ja.properties";	// アプリケーション・メッセージプロパティファイル
	var LABEL_JA_PROPERTIES_FILENAME = "label_ja.properties";				// ラベル・メッセージプロパティファイル
	
	// 出力ファイル関連
	var BASE_FILENAME = "雛形_メッセージプロパティファイル仕様書.xlsb";		// 雛形ファイル名
	var OUTPUT_FILENAME = "メッセージプロパティファイル仕様書";			// 出力ファイル名
	
	var APPLICATION_JA_PROPERTIES_SHEET_NAME = 'アプリケーション・メッセージプロパティファイル';	//出力シート名
	var LABEL_JA_PROPERTIES_SHEET_NAME = 'ラベル・メッセージプロパティファイル';				//出力シート名
	var OUT_START_ROW = 3;	//出力開始行
	
	
	/*
	 * 入力ファイル存在チェック
	 */
	var objFso = new ActiveXObject('Scripting.FileSystemObject');
	if (src_rootFullPath.substr(src_rootFullPath.length-1,1) !== '/') {
		src_rootFullPath = src_rootFullPath + '/';
	}
	if ( !objFso.FileExists( src_rootFullPath + LABEL_JA_PROPERTIES_FILENAME ) ){
		//入力ファイルが存在しない場合、処理中断
		WScript.Echo('異常終了:\n'
					+ LABEL_JA_PROPERTIES_FILENAME + 'ファイルが存在しません。\n'
					+ 'このスクリプトファイル内の環境変数の値を確認して下さい。');
		objFso = null;
		WScript.Quit();
	}
	
	
	
	/*
	 * メイン処理
	 *   メッセージプロパティファイルから設計書を生成
	 */
	
	// 雛形から出力ファイル生成し、出力ファイルへ書き込み
	var objShell = new ActiveXObject('WScript.Shell');
	var filePath_new = objShell.CurrentDirectory + '/output/' + OUTPUT_FILENAME;
	objFso.getFile(objShell.CurrentDirectory + '/' + BASE_FILENAME).Copy(filePath_new + '_new.xlsb', true);
	objShell = null;

	var objExcel = new ActiveXObject('Excel.Application');
	var doc_new = objExcel.Workbooks.Open(objFso.GetAbsolutePathName(filePath_new + '_new.xlsb'));
	doc_new.CheckCompatibility = false;
	objExcel.EnableEvents = false;
	objExcel.ScreenUpdating = false;
	
	// ラベル・メッセージプロパティファイル
	msg_properties_file_to_doc(src_rootFullPath + LABEL_JA_PROPERTIES_FILENAME
								, doc_new
								, LABEL_JA_PROPERTIES_SHEET_NAME
								, OUT_START_ROW);
	// アプリケーション・メッセージプロパティファイル
	msg_properties_file_to_doc(src_rootFullPath + APPLICATION_JA_PROPERTIES_FILENAME
								, doc_new
								, APPLICATION_JA_PROPERTIES_SHEET_NAME
								, OUT_START_ROW);
	
	
	/*
	 * 後処理
	 */
		
	// 一旦ファイル保存し、再度開き補正する
	doc_new.SaveAs(filePath_new + '.xls', 56);
	doc_new.Close();
	
	doc_new = objExcel.Workbooks.Open(objFso.GetAbsolutePathName(filePath_new + '.xls'));
	
	doc_new.Sheets('表紙').Activate();
	objExcel.Range("A1").Select();

	// ファイル保存
	doc_new.SaveAs(filePath_new + '.xlsx', 51);
	doc_new.Close();
	doc_new = null;
	
	// ごみ削除
	objFso.DeleteFile(filePath_new + '.xls');
	objFso.DeleteFile(filePath_new + '_new.xlsb');
	objFso = null;
	
	objExcel.EnableEvents = true;
	objExcel.ScreenUpdating = true;
	objExcel.Quit();
	objExcel = null;
	WScript.Echo('正常終了');



	
	/*
	 * メッセージプロパティファイルから設計書を生成
	 */
	function msg_properties_file_to_doc(file_name, doc_new, sheet_name, out_start_row) {
		var old_data = {};
		var old_key;
		
		// メッセージプロパティファイル 情報を一旦配列に格納
		var sr = new ActiveXObject("ADODB.Stream");
		sr.Type = 2;	// テキスト形式
		sr.charset = "utf-8";	//"Shift_JIS", "_autodetect_all"
		sr.Open();
		
		sr.LoadFromFile( file_name );
		var in_data = "";
		var in_line = 1;
		while (!sr.EOS) {
			in_data = sr.ReadText( -2 );	// 一行ごと

			if (in_data.replace(/^\s+|\s+$/g,'').length < 1) {
			//空行は無視
			} else if (in_data.substr(0, 3) === '## ') {
			// ヘッダの場合
				// 種類 項目設定
				old_key = in_data.substr(3).replace(/^\s+|\s+$/g,'');
				old_data[old_key] = new Array();
			} else if (in_data.substr(0, 1) === '#') {
			// コメントの場合は無視
			} else {
			// 有効行
				// 分解
				var words = new Array();
				var pos = in_data.indexOf('=');
				words.push( in_data.substr(0, pos) );
				words.push( in_data.substr(pos + 1) );
				
				/*
				 * チェック処理
				 */
					// ラベル・メッセージプロパティファイル の場合
					if (sheet_name === 'ラベル・メッセージプロパティファイル') {
						
						// メッセージID
							// 末尾2文字チェック
							if (words[0].substr( words[0].length-2 )  !== '.G' ){
								// データ不正
								WScript.Echo('異常終了：メッセージID末尾2文字が「.G」でないため修正願います。\n'
											+ '  file=' + file_name + '\n'
											+ '  line=' + in_line + '\n'
											+ '  data=' + in_data);
								sr.Close();
								sr = null;
								objFso = null;
								WScript.Quit();
							}
						
/* ☆確認待ち
						// メッセージID
							//メッセージに途中で折り返し（<br/>）を含む場合、「.NOWRAP」存在チェック
							if (words[1].indexOf('=')( words[1].length-2 )  !== '.NOWRAP' ){
								// データ不正
								WScript.Echo('異常終了：メッセージID末尾2文字が「.G」でないため修正願います。\n'
											+ '  file=' + file_name + '\n'
											+ '  line=' + in_line + '\n'
											+ '  data=' + in_data);
								sr.Close();
								sr = null;
								objFso = null;
								WScript.Quit();
							}
*/							

					}
				
				
				// メッセージID 項目設定
				old_data[old_key].push( words[0] );
				
				// メッセージ 項目設定
				old_data[old_key].push( words[1] );
			}
			
			in_line++;
		}
		sr.Close();
		sr = null;
		
		
		// データ数分 出力行を追加
		var row_cnt = 0;
		for (var key in old_data) {
			row_cnt += (old_data[key].length / 2);
		}
		doc_new.Sheets(sheet_name).Rows(out_start_row + 1 ).Copy;	// 罫線が正しい2行目をコピー
		doc_new.Sheets(sheet_name).Rows((out_start_row + 1) + ':' + (out_start_row + row_cnt - 3) ).Insert;	// 必要行数分追加
		
		// 出力
		var out_row = out_start_row;
		for (var key in old_data) {
			// 種類 項目設定
			doc_new.Sheets(sheet_name).Cells(out_row, 'B').Value = key + '\n';	//見やすいように改行追加
			
			for ( var i = 0; i < old_data[key].length; i=i+2 ) {
				// メッセージID 項目設定
				doc_new.Sheets(sheet_name).Cells(out_row, 'C').Value = old_data[key][i];
				// メッセージ 項目設定
				doc_new.Sheets(sheet_name).Cells(out_row, 'D').Value = old_data[key][i+1] + '\n';	//見やすいように改行追加
				
				out_row++;
			}
		}
		
		return;
	};

})();
